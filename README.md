A bash script file that can be used to set different CPU modes for the Hak5 Key Croc. Actually more or less a "rip off" using the cucumber extension for the Hak5 Bash Bunny with some slight modifications.

NOTE! This way of "tweaking" the CPU isn't officially supported by Hak5 (since I haven't found built in support to control it on the Croc like it's done on the Bunny). Using this script is at your own risk.

More information about the cucumber extension for the Bunny can be found here
https://docs.hak5.org/bash-bunny/writing-payloads/cpu-control
