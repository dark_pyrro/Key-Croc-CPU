#!/bin/bash

# Check that not more than one (1) command line parameter/argument was passed to the script
if [ $# -gt 1 ]
  then
    echo "Too many arguments passed to the script. Only one (1) allowed. Exiting..."
    exit
fi
# Checking that an argument has been passed to the script
if [ -z "$1" ]
  then
    echo "No CPU mode supplied. ENABLE, DISABLE or PLAID is valid. Exiting..."
    exit
fi

case $1 in
"ENABLE")
  echo ondemand | tee /sys/devices/system/cpu/cpu{0..3}/cpufreq/scaling_governor &> /dev/null
  echo 0 | tee /sys/devices/system/cpu/cpu{1..3}/online &> /dev/null
  ;;
"DISABLE")
  echo 1 | tee /sys/devices/system/cpu/cpu{1..3}/online &> /dev/null
  sleep 2
  echo ondemand | tee /sys/devices/system/cpu/cpu{0..3}/cpufreq/scaling_governor &> /dev/null
  ;;
"PLAID")
  echo 1 | tee /sys/devices/system/cpu/cpu{1..3}/online &> /dev/null
  sleep 2
  echo performance | tee /sys/devices/system/cpu/cpu{0..3}/cpufreq/scaling_governor &> /dev/null
  ;;
*)
  echo "Invalid CPU mode ($1) supplied. Exiting..."
  exit 1
esac
